<?php
  if (!isset($_GET['i']))
    invalid_args();
  
  // Use the libs from the Sports dir
  $_LIB_DIR = '/var/www/debear/homebrew/sports/htdocs';
  require $_LIB_DIR . '/libraries/array.inc';
  require $_LIB_DIR . '/libraries/ad_hoc/cdn.inc';
  require $_LIB_DIR . '/libraries/ad_hoc/image.inc';
  date_default_timezone_set('Europe/London');

  // Decode
  $_GET['i'] = cdn_url_decode($_GET['i']);
  if (strpos($_GET['i'], '='))
    parse_str($_GET['i'], $_GET);
  // Make absolute links relative, as we are already in the root dir...
  $_GET['i'] = ltrim($_GET['i'], '/');

  // Debug...?
  //print '<pre>'; var_export($_GET); print '</pre>'; exit;

  // Does the image exist?
  if (!file_exists($_GET['i']))
    invalid_args();

  // Validation...
  if (isset($_GET['rs']) && $_GET['rs'])
    // Width and Height args?
    $_GET['rs'] = (isset($_GET['w']) && is_numeric($_GET['w']) && isset($_GET['h']) && is_numeric($_GET['h']));

  // Browser caching?
  session_cache_limiter(false);
  header('Cache-Control: private, max-age=43200'); // 43200 = 12 hours
  $headers = apache_request_headers();
  $image_mtime = filemtime($_GET['i']);
  $image_mtime_head = header_date($image_mtime);
  if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) >= $image_mtime)) {
    header('Last-Modified: ' . $image_mtime_head, true, 304);
    exit;
  }
  // Image isn't cached, so add time to headers
  header('Last-Modified: ' . $image_mtime_head, true, 200);
  header('Expires: ' . header_date(strtotime('+1 week')));

  /* Process... */
  // Is what we're being asked to output the original image? If so, no need to resize
  if (isset($_GET['rs'])) {
    list($img_src_w, $img_src_h, $img_src_type) = getimagesize($_GET['i']);
    if ($img_src_w == $_GET['w'] && $img_src_h == $_GET['h'])
      unset($_GET['rs']);
  }

  // Raw image to output?
  if (!isset($_GET['rs']) || !$_GET['rs']) {
    // Get the MIME type from the extension
    $ext = substr($_GET['i'], strrpos($_GET['i'], '.') + 1);
    if ($ext == 'jpg') $ext = 'jpeg';
    // Now format the output
    header('Content-type: image/' . $ext);
    header('Content-Length: ' . filesize($_GET['i']));
    readfile($_GET['i']);

  // Resize the image...
  } else
    resize_image($_GET['i'], $_GET['w'], $_GET['h'],
                array('preserve_ratio' => is_array_index($_GET, 'preserve', 'bool'),
                      'crop' => is_array_index($_GET, 'crop', 'bool'),
                      'offset_adjust' => is_array_index($_GET, 'offset') ? $_GET['offset'] : null,
                      'ext_out' => is_array_index($_GET, 'ext') ? $_GET['ext'] : null));

  // How to feedback the error: a 404
  function invalid_args() {
    http_response_code(404);
    exit;
  }
  function header_date($ts) {
    return gmdate('D, d M Y H:i:s', $ts) . ' GMT';
  }
?>
