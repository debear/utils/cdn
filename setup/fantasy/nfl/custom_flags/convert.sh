basedir=`dirname $0`
cd $basedir/src

for img in `ls *`
do
  img_orig=$img
  img_name=`echo "$img" | sed -r 's/^(.*?)\.[^\.]+$/\1/'`

  size=`file "$img" | grep -Po ' \d+ x \d+,'`
  width=`echo "$size" | sed -r 's/^ ([0-9]+) x [0-9]+,$/\1/'`
  height=`echo "$size" | sed -r 's/^ [0-9]+ x ([0-9]+),$/\1/'`
  ratio_test=`echo "($width / $height) >= (256 / 199)" | bc -l`

  # Create the basic canvas with which to resize
  if [ $ratio_test -eq 1 ]; then
    convert $img -resize 256x /tmp/$img_name.png
  else
    convert $img -resize x199 /tmp/$img_name.png
  fi

#  convert /tmp/$img_name.png -matte -bordercolor none -border 128x100 -gravity center -crop 256x199+0+0 /tmp/$img_name.png
  convert /tmp/$img_name.png -background none -gravity center -extent 256x199 /tmp/$img_name.png
  img=/tmp/$img_name.png

  # 16x11
#  convert $img -resize 16x11\! ../16/$img_name.png
  convert $img -gamma 0.454545 -resize 16x11\! -gamma 2.2 ../16/$img_name.png

  # 32x25
#  convert $img -resize 32x25\! ../32/$img_name.png

  # 48x38
  convert $img -resize 48x38\! ../48/$img_name.png

  # 64x50
  convert $img -resize 64x50\! ../64/$img_name.png  

  # 128x100
  convert $img -resize 128x100\! ../128/$img_name.png  

  # 256x199
#  convert $img -resize 256x199\! ../256/$img_name.png  
done

# Copy from here to the Common sites area
cd ..
for d in `ls -d * | grep -P '^[0-9]+$'`; do cp --preserve $d/*.png /var/www/debear/sites/common/flags/$d/; done
