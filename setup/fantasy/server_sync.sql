SET @sync_app_base := 'cdn_fantasy';

##############
## Combined ##
##############

SET @sync_app := @sync_app_base;
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'All Fantasy CDN', 'dev,live', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Fantasy > F1', 1, 'script'),
                        (@sync_app, 2, 'CDN > Fantasy > MotoGP', 2, 'script'),
                        (@sync_app, 3, 'CDN > Fantasy > SGP', 3, 'script'),
                        (@sync_app, 4, 'CDN > Fantasy > NFL', 4, 'script');
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                        VALUES (@sync_app, 1, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_f1 __REMOTE__')),
                               (@sync_app, 2, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_motogp __REMOTE__')),
                               (@sync_app, 3, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_sgp __REMOTE__')),
                               (@sync_app, 4, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_nfl __REMOTE__'));

################
## Motorsport ##
################

##
## Formula 1
##
SET @sync_app := CONCAT(@sync_app_base, '_f1');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'F1 Fantasy CDN', 'dev,live', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Fantasy > F1', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/fantasy/f1', 'debear/sites/cdn/htdocs/fantasy/f1', '--exclude="*/static/*/*/*.png"');

##
## MotoGP
##
SET @sync_app := CONCAT(@sync_app_base, '_motogp');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'MotoGP Fantasy CDN', 'dev,live', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Fantasy > MotoGP/2/3', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/fantasy/motogp', 'debear/sites/cdn/htdocs/fantasy/motogp', '--exclude="*/static/*/*/*.png"');

##
## Speedway Grand Prix
##
SET @sync_app := CONCAT(@sync_app_base, '_sgp');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'SGP Fantasy CDN', 'dev,live', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Fantasy > SGP', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/fantasy/sgp', 'debear/sites/cdn/htdocs/fantasy/sgp', '--exclude="*/static/*/*/*.png"');

##################
## Major League ##
##################

##
## NFL
##
SET @sync_app := CONCAT(@sync_app_base, '_nfl');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'NFL Fantasy CDN', 'dev,live', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Fantasy > NFL', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/fantasy/nfl', 'debear/sites/cdn/htdocs/fantasy/nfl', '--exclude="*/static/*/*/*.png"');

