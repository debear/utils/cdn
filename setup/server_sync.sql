SET @sync_app := 'cdn';

#
# Full Sync
#
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'CDN', 'dev,data', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ DeBear CDN', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs', 'debear/sites/cdn/htdocs', '--exclude="fantasy/*/*/static/*/*/*.png"');

