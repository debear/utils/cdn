SET @sync_app_base := 'cdn_sports';

##############
## Combined ##
##############

SET @sync_app := @sync_app_base;
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'All Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > Tour', 1, 'script'),
                        (@sync_app, 2, 'CDN > Sports > Giro', 2, 'script'),
                        (@sync_app, 3, 'CDN > Sports > Vuelta', 3, 'script'),
                        (@sync_app, 4, 'CDN > Sports > NFL', 4, 'script'),
                        (@sync_app, 5, 'CDN > Sports > NHL', 5, 'script'),
                        (@sync_app, 6, 'CDN > Sports > AHL', 6, 'script'),
                        (@sync_app, 7, 'CDN > Sports > EIHL', 7, 'script'),
                        (@sync_app, 8, 'CDN > Sports > MLB', 8, 'script'),
                        (@sync_app, 9, 'CDN > Sports > F1', 9, 'script'),
                        (@sync_app, 10, 'CDN > Sports > MotoGP/2/3', 10, 'script'),
                        (@sync_app, 11, 'CDN > Sports > SGP', 11, 'script');
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                        VALUES (@sync_app, 1, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_tdf __REMOTE__')),
                               (@sync_app, 2, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_gdi __REMOTE__')),
                               (@sync_app, 3, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_vae __REMOTE__')),
                               (@sync_app, 4, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_nfl __REMOTE__')),
                               (@sync_app, 5, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_nhl __REMOTE__')),
                               (@sync_app, 6, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_ahl __REMOTE__')),
                               (@sync_app, 7, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_eihl __REMOTE__')),
                               (@sync_app, 8, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_mlb __REMOTE__')),
                               (@sync_app, 9, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_f1 __REMOTE__')),
                               (@sync_app, 10, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_motogp __REMOTE__')),
                               (@sync_app, 11, '/git/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app_base, '_sgp __REMOTE__'));

#############
## Cycling ##
#############

##
## Tour de France
##
SET @sync_app := CONCAT(@sync_app_base, '_tdf');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Tour Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > Tour', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/sports/tdf', 'debear/sites/cdn/htdocs/sports/tdf', NULL);

##
## Giro dItalia
##
SET @sync_app := CONCAT(@sync_app_base, '_gdi');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Giro Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > Giro', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/sports/gdi', 'debear/sites/cdn/htdocs/sports/gdi', NULL);

##
## Vuelta a Espana
##
SET @sync_app := CONCAT(@sync_app_base, '_vae');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Vuelta Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > Vuelta', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/sports/vae', 'debear/sites/cdn/htdocs/sports/vae', NULL);

##################
## Major League ##
##################

##
## NFL
##
SET @sync_app := CONCAT(@sync_app_base, '_nfl');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'NFL Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > NFL', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/sports/nfl', 'debear/sites/cdn/htdocs/sports/nfl', NULL);

##
## NHL
##
SET @sync_app := CONCAT(@sync_app_base, '_nhl');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'NHL Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > NHL', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/sports/nhl', 'debear/sites/cdn/htdocs/sports/nhl', NULL);

##
## AHL
##
SET @sync_app := CONCAT(@sync_app_base, '_ahl');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'AHL Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > AHL', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/sports/ahl', 'debear/sites/cdn/htdocs/sports/ahl', NULL);

##
## Elite League
##
SET @sync_app := CONCAT(@sync_app_base, '_eihl');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'EIHL Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > EIHL', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/sports/eihl', 'debear/sites/cdn/htdocs/sports/eihl', NULL);

##
## MLB
##
SET @sync_app := CONCAT(@sync_app_base, '_mlb');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'MLB Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > MLB', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/sports/mlb', 'debear/sites/cdn/htdocs/sports/mlb', NULL);

################
## Motorsport ##
################

##
## Formula 1
##
SET @sync_app := CONCAT(@sync_app_base, '_f1');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'F1 Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > F1', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/sports/f1', 'debear/sites/cdn/htdocs/sports/f1', NULL);

##
## MotoGP/2/3
##
SET @sync_app := CONCAT(@sync_app_base, '_motogp');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'MotoGP/2/3 Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > MotoGP/2/3', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/sports/motogp', 'debear/sites/cdn/htdocs/sports/motogp', NULL);

##
## Speedway Grand Prix
##
SET @sync_app := CONCAT(@sync_app_base, '_sgp');
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'SGP Sports CDN', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'CDN > Sports > SGP', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 1, '/var/www/debear/sites/cdn/htdocs/sports/sgp', 'debear/sites/cdn/htdocs/sports/sgp', NULL);

